// 2015110705 김민규
// 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRING_MAX 11
#define BUCKET_MAX 11

typedef unsigned int UINT;

typedef struct {
	char		item[STRING_MAX];
	UINT		key;
} Element;

typedef struct node* nodePointer;
typedef struct node{
	Element		data;
	nodePointer	link;
} Node;

nodePointer* hashTable; //해시테이블

UINT h(UINT);
UINT stringToInt(char*);
void insert(nodePointer);
Element* search(UINT);
void printTable();

int main()
{
	FILE* pInputFile = fopen("input.txt", "r");
	char buffer[STRING_MAX];
	int i;

	hashTable = (nodePointer*)malloc(sizeof(nodePointer) * BUCKET_MAX);

	for (i = 0; i < BUCKET_MAX; i++) {
		hashTable[i] = NULL;
	}

	printf("input strings ");
	while (!feof(pInputFile)) {
		nodePointer newNode = (nodePointer)malloc(sizeof(Node));
		fscanf(pInputFile, "%s", buffer);
		
		strcpy(newNode->data.item, buffer);
		newNode->data.key = stringToInt(buffer);
		newNode->link = NULL;

		insert(newNode);
		printf("%s ", buffer);
	}
	printf("\n\n");

	fclose(pInputFile);

	printTable();

	while (1) {
		printf("\ninput ^Z to quit \n");
		printf("string to search >> ");
		if (scanf("%s", buffer) == -1) {
			break;
		}
		search(stringToInt(buffer));
	}

	return 0;
}

UINT h(UINT key)
{
	return key % BUCKET_MAX;
}

UINT stringToInt(char* key)
{
	UINT number = 0;

	while(*key) {
		number += *key++;
	}

	return number;
}

void insert(nodePointer newNode)
{
	nodePointer current;
	UINT homeBucket = h(newNode->data.key);

	if (hashTable[homeBucket] == NULL) {
		hashTable[homeBucket] = newNode;
		return;
	}

	for (current = hashTable[homeBucket]; current; current = current->link) {
		if (current->data.key == newNode->data.key) {
			fprintf(stderr, "중복된 key 값이 존재합니다.");
			exit(EXIT_FAILURE);
		}

		if (current->link == NULL) {
			current->link = newNode;
			break;
		}
	}
}

Element* search(UINT key)
{
	nodePointer current;
	UINT homeBucket = h(key);
	UINT count = 1;

	for (current = hashTable[homeBucket]; current; current = current->link) {
		count++;
		if (current->data.key == key) {
			printf("item : %s, key : %d, the number of comparisions : %d \n", 
				current->data.item, current->data.key, count);
			return &current->data;
		}
	}

	printf("it doesn't exist! \n");
	return NULL;
}

void printTable()
{
	int i;
	nodePointer iter;

	printf("%8s%6s %3s\n", " ", "item", "key");

	for (i = 0; i < BUCKET_MAX; i++) {
		printf("ht[%2d] : ", i);
		for (iter = hashTable[i]; iter != NULL; iter = iter->link) {
			printf("<%s %d> ", iter->data.item, iter->data.key);
		}
		printf("\n");
	}
}